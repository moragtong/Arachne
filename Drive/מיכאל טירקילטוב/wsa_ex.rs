use std;
use winapi;

use winapi::{
	um::winsock2::*,
	shared::ws2def::*,
};

use std::{
	os::windows::io::AsRawSocket,
	io::Write,
	net::*
};

const WSAIDS: [winapi::shared::guiddef::GUID; 8] = [
	winapi::shared::guiddef::GUID {
		Data1:	0xb5367df1,
		Data2:	0xcbac,
		Data3:	0x11cf,
		Data4:	[0x95, 0xca, 0x00, 0x80, 0x5f, 0x48, 0xa1, 0x92]
	},
	winapi::shared::guiddef::GUID {
		Data1:	0x25a207b9,
		Data2:	0xddf3,
		Data3:	0x4660,
		Data4:	[0x8e, 0xe9, 0x76, 0xe5, 0x8c, 0x74, 0x06, 0x3e]
	},
	winapi::shared::guiddef::GUID {
		Data1:	0x7fda2e11,
		Data2:	0x8630,
		Data3:	0x436f,
		Data4:	[0xa0, 0x31, 0xf5, 0x36, 0xa6, 0xee, 0xc1, 0x57]
	},
	winapi::shared::guiddef::GUID {
		Data1:	0xb5367df2,
		Data2:	0xcbac,
		Data3:	0x11cf,
		Data4:	[0x95, 0xca, 0x00, 0x80, 0x5f, 0x48, 0xa1, 0x92]
	},
	winapi::shared::guiddef::GUID {
		Data1:	0xb5367df0,
		Data2:	0xcbac,
		Data3:	0x11cf,
		Data4:	[0x95, 0xca, 0x00, 0x80, 0x5f, 0x48, 0xa1, 0x92]
	},
	winapi::shared::guiddef::GUID {
		Data1:	0xd9689da0,
		Data2:	0x1f90,
		Data3:	0x11d3,
		Data4:	[0x99, 0x71, 0x00, 0xc0, 0x4f, 0x68, 0xc8, 0x76]
	},
	winapi::shared::guiddef::GUID {
		Data1:	0xf689d7c8,
		Data2:	0x6f1f,
		Data3:	0x436b,
		Data4:	[0x8a, 0x53, 0xe5, 0x4f, 0xe3, 0x51, 0xc3, 0x22]
	},
	winapi::shared::guiddef::GUID {
		Data1:	0xa441e712,
		Data2:	0x754f,
		Data3:	0x43ca,
		Data4:	[0x84, 0xa7, 0x0d, 0xee, 0x44, 0xcf, 0x60, 0x6d]
	},
];

#[repr(C)]
#[derive(Copy, Clone)]
struct TransmitFileBuffers {
	head:			*mut std::os::raw::c_void,
	head_length:	u32,
	tail:			*mut std::os::raw::c_void,
	tail_length:	u32,
}

#[repr(C)]
#[derive(Copy, Clone)]
union large_integer {
	u:			(u32, i32),
	quad_part:	i64,
}

#[repr(C)]
#[derive(Copy, Clone)]
struct file_seek_info {
	offset:	large_integer,
	hfile:	*mut std::os::raw::c_void,
}

#[repr(C)]
#[derive(Copy, Clone)]
union transmit_packets_internal {
	file:	file_seek_info,
	buffer:	*mut std::os::raw::c_void,
}

#[repr(C)]
#[derive(Copy, Clone)]
struct TransmitPacketsElement {
	flags:		u32,
	length:		u32,
	internal:	transmit_packets_internal,
}

union Uninitialized<T: Copy> {
	o:		T,
	uninitialized:	()
}

#[repr(C)]
struct WsaLib {
	accept:					Uninitialized<unsafe extern "system" fn(usize, usize, *mut std::os::raw::c_void, u32, u32, u32, *mut u32, *mut WSAOVERLAPPED)>,
	connect:				Uninitialized<unsafe extern "system" fn(usize, *const SOCKADDR, i32, *mut std::os::raw::c_void, u32, *mut u32, *mut WSAOVERLAPPED)>,
	disconnect:				Uninitialized<unsafe extern "system" fn(usize, *mut WSAOVERLAPPED, u32, u32)>,
	get_accept_sockaddrs:	Uninitialized<unsafe extern "system" fn(*mut std::os::raw::c_void, u32, u32, u32, *mut *mut SOCKADDR, *mut i32, *mut *mut SOCKADDR, *mut i32)>,
	transmit_file:			Uninitialized<unsafe extern "system" fn(usize, *mut std::os::raw::c_void, u32, u32, *mut WSAOVERLAPPED, *mut TransmitFileBuffers, u32)>,
	transmit_packets:		Uninitialized<unsafe extern "system" fn(usize, *mut TransmitPacketsElement, u32, u32, *mut WSAOVERLAPPED, u32)>,
	recv_msg:				Uninitialized<unsafe extern "system" fn(usize, *mut WSAMSG, *mut u32, *mut WSAOVERLAPPED, LPWSAOVERLAPPED_COMPLETION_ROUTINE)>,
	send_msg:				Uninitialized<unsafe extern "system" fn(usize, *mut WSAMSG, u32, *mut u32, *mut WSAOVERLAPPED, LPWSAOVERLAPPED_COMPLETION_ROUTINE)>,
}

static mut WSA: WsaLib = WsaLib {
	accept:					Uninitialized { uninitialized: () },
	connect:				Uninitialized { uninitialized: () },
	disconnect:				Uninitialized { uninitialized: () },
	get_accept_sockaddrs:	Uninitialized { uninitialized: () },
	transmit_file:			Uninitialized { uninitialized: () },
	transmit_packets:		Uninitialized { uninitialized: () },
	recv_msg:				Uninitialized { uninitialized: () },
	send_msg:				Uninitialized { uninitialized: () },
};

fn initialize_wsalib(sock: u32) {
	unsafe {
		let mut bytes = 0;
		for (guid, func) in WSAIDS.iter().zip(std::mem::transmute::<_, &mut [&(); 8]>(&mut WSA)) {
			WSAIoctl(sock as _, SIO_GET_EXTENSION_FUNCTION_POINTER,
			std::mem::transmute(guid), std::mem::size_of::<winapi::shared::guiddef::GUID>() as _,
			func as *mut _ as _, std::mem::size_of::<&()>() as _, &mut bytes, 0 as _, None);
		}
	}
}