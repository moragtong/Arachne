use std::io::Write;
use std::io::Read;

struct TextNum {
	i:	usize,
	id:	[u8; 9],
}

impl Default for TextNum {
	fn default() -> Self {
		Self {
			i:	0,
			id:	[b'0'; 9],
		}
	}
}

impl TextNum {
	fn inc(&mut self) -> &[u8; 9] {
		let i = self.i;
		if *unsafe { self.id.get_unchecked(i) } == 9 {
			if i == 8 {
				*self = Default::default();
			} else {
				let digit = unsafe { self.id.get_unchecked_mut(i) };
				*digit = b'0';
				self.i += 1;
			}
		} else {
			let digit = unsafe { self.id.get_unchecked_mut(i) };
			*digit += 1;
		}
		&self.id
	}
}

#[derive(Default)]
struct Database {
	by_id:		std::collections::HashMap<[u8; 9], [u8; 15]>,
	by_addr:	std::collections::HashMap<[u8; 15], [u8; 9]>,
	text_num:	TextNum,
}

impl Database {
	pub fn get(&self, id: &[u8; 9]) -> Option<&[u8; 15]> {
		self.by_id.get(id)
	}

	pub fn insert(&mut self, addr: [u8; 15]) -> Option<&[u8; 9]> {
		let Self { by_id, by_addr, text_num } = self;
		if let std::collections::hash_map::Entry::Vacant(mut entry) = by_addr.entry(addr) {
			let id = text_num.inc();
			entry.insert(*id);
			by_id.insert(*id, addr);
			println!("new client: address: {}, updated ip dict: {:?}", unsafe { std::str::from_utf8_unchecked(&addr) }, by_id);
			Some(id)
		} else {
			None
		}
	}

	pub fn remove(&mut self, id: &[u8; 9]) -> Option<[u8; 15]> {
		let Self { by_id, by_addr, .. } = self;
		if let Some(ret) = by_id.remove(id) {
			by_addr.remove(&ret);
			Some(ret)
		} else {
			None
		}
	}
}

fn main() -> std::io::Result<()> {
	use std::net::*;
	let database = std::sync::Arc::new(std::sync::RwLock::new(Database::default()));
	let id_generator = TcpListener::bind((Ipv4Addr::from(0), 5555))?;
	let ip_exchanger = TcpListener::bind(SocketAddrV4::new(Ipv4Addr::from(0), 5556))?;
	let disconnector = TcpListener::bind(SocketAddrV4::new(Ipv4Addr::from(0), 5557))?;

	let cloned = database.clone();
	std::thread::Builder::new().spawn(move || {
		println!("new clients");
		for stream in id_generator.incoming() {
			match stream {
				Ok(mut client) => {
					let mut addr = [0; 15];
					if let Err(err) = client.read_exact(&mut addr) {
						println!("recv err: {:?}", err);
					} else {
						client.write(cloned.write().unwrap().insert(addr).unwrap_or(b"EXIST0000"));
					}
				}
				Err(err) => println!("new clients: {:?}", err)
			}
		}
	})?;

	let cloned = database.clone();
	std::thread::Builder::new().spawn(move || {
		println!("ip exchange");
		loop {
			match ip_exchanger.accept() {
				Ok((mut client, addr)) => {
					let mut id = [0; 9];
					if let Err(err) = client.read_exact(&mut id) {
						println!("recv err: {:?}", err);
					} else {
						println!("---- exchange: {}, requested ip of: {}", unsafe { std::str::from_utf8_unchecked(&id) }, addr);
						client.write(cloned.read().unwrap().get(&id).unwrap_or(b"ERROR0000000000"));
					}
				}
				Err(err) => println!("exchange ip: {:?}", err)
			}
		}
	})?;

	println!("disconnections");
	for stream in disconnector.incoming() {
		match stream {
			Ok(mut client) => {
				let mut id = [0; 9];
				if let Err(err) = client.read_exact(&mut id) {
					println!("recv err: {:?}", err);
				} else {
					if let Some(addr) = database.write().unwrap().remove(&id) {
						println!("---- exchange: {}, requested ip of: {}", unsafe { std::str::from_utf8_unchecked(&id) }, unsafe { std::str::from_utf8_unchecked(&addr) });
						client.write(database.read().unwrap().by_id.get(&id).unwrap_or(b"ERROR0000000000"));
					}
				}
			}
			Err(err) => println!("handle disconnect: {:?}", err)
		}
	}

	Ok(())
}