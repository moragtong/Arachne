extern crate winapi;

use winapi::shared::ws2def::SOCKADDR;
use winapi::shared::ws2def::WSABUF;
use winapi::shared::ws2def::SIO_GET_EXTENSION_FUNCTION_POINTER;
use winapi::shared::ws2def::AF_INET;
use winapi::um::winsock2::*;

use std::{
	io::Read,
	os::windows::io::IntoRawSocket,
	net::*
};

fn get_sys_info() -> winapi::um::sysinfoapi::SYSTEM_INFO {
	unsafe {
		let mut sysinfo = std::mem::uninitialized();
		winapi::um::sysinfoapi::GetSystemInfo(&mut sysinfo);
		sysinfo
	}
}

#[derive(Debug)]
struct StaticPacket<T> {
	size:	usize,
	data:	T,
}

impl<T: Default> Default for StaticPacket<T> {
	fn default() -> Self {
		StaticPacket {
			size:	Default::default(),
			data:	Default::default(),
		}
	}
}

impl<T> std::convert::AsRef<[u8]> for StaticPacket<T> {
	fn as_ref(&self) -> &[u8] {
		unsafe { std::mem::transmute((self, self.size + std::mem::size_of::<usize>())) }
	}
}

#[repr(C)]
struct AcceptData {
	overlapped:	WSAOVERLAPPED,
	sock:		SOCKET,
	data:		[u8; std::mem::size_of::<SOCKADDR>() * 2 + 32],
}

impl Default for AcceptData {
	fn default() -> Self {
		unsafe { Self {
			sock: socket(AF_INET, SOCK_STREAM, 0),
			overlapped: std::mem::zeroed(),
			.. std::mem::uninitialized()
		} }
	}
}

struct OperationRaw {
	_tag:	usize,
	data:	(usize, usize)
}

enum Operation {
	None,
	RecvSize(usize),
	Recv(Box<[u8]>),
}

impl AsRef<OperationRaw> for Operation {
	fn as_ref(&self) -> &OperationRaw {
		unsafe { std::mem::transmute(self) }
	}
}

impl AsMut<OperationRaw> for Operation {
	fn as_mut(&mut self) -> &mut OperationRaw {
		unsafe { std::mem::transmute(self) }
	}
}

impl Default for Operation {
	fn default() -> Self {
		Operation::None
	}
}

#[repr(C)]
struct OverlappedExInner {
	overlapped: WSAOVERLAPPED,
	operation:	Operation,
}

impl OverlappedExInner {
	pub fn new(operation: Operation) -> Self {
		Self {
			overlapped:	unsafe { std::mem::zeroed() },
			operation,
		}
	}
}

impl Default for OverlappedExInner {
	fn default() -> Self {
		Self::new(Default::default())
	}
}

#[derive(Default)]
struct OverlappedEx(Box<OverlappedExInner>);

impl OverlappedEx {
	pub fn send_static(self, sock: SOCKET, wsabuf: &WsaBuf) {
		unsafe { 
			WSASend(sock as _, &wsabuf.0 as *const _ as _, 1, 0 as _, 0,
				Box::into_raw(self.0) as _, None);
		}
	}

	pub fn recv(mut self, sock: SOCKET) {
		*self.0 = OverlappedExInner::new(Operation::RecvSize(0));
		let mut wsabuf = {
			let raw = self.0.operation.as_mut();
			WSABUF { len: std::mem::size_of::<usize>() as _, buf: &mut raw.data.0 as *mut _ as _ }
		};

		let mut flags = MSG_WAITALL as u32;
		unsafe { WSARecv(sock as _, &mut wsabuf, 1, 0 as _, &mut flags, Box::into_raw(self.0) as _, None) };
	}
}

struct IOCompletionPort(usize);

unsafe impl Send for IOCompletionPort {}

impl Default for IOCompletionPort {
	fn default() -> Self {
		IOCompletionPort(unsafe { winapi::um::ioapiset::CreateIoCompletionPort(-1 as _, 0 as _, 0, 0) } as _)
	}
}

impl Drop for IOCompletionPort {
	fn drop(&mut self) {
		unsafe { winapi::um::handleapi::CloseHandle(self.0 as _) };
	}
}

impl IOCompletionPort {
	pub fn new(sock: SOCKET) -> Self {
		IOCompletionPort(unsafe { winapi::um::ioapiset::CreateIoCompletionPort(sock as _, 0 as _, sock as _, 0) } as _)
	}

	pub fn associate(&self, sock: SOCKET) {
		unsafe { winapi::um::ioapiset::CreateIoCompletionPort(sock as _, self.0 as _, sock as _, 0) };
	}

	pub fn get_queued_completion_status(&self) -> Option<(u32, SOCKET, std::ptr::NonNull<WSAOVERLAPPED>)> {
		unsafe {
			let (mut len, mut sock, mut overlapped) = std::mem::zeroed();
			winapi::um::ioapiset::GetQueuedCompletionStatus(self.0 as _, &mut len, &mut sock, &mut overlapped, winapi::um::winbase::INFINITE);
			if let Some(overlapped) = std::ptr::NonNull::new(overlapped) {
				Some((len, sock, overlapped))
			} else {
				None
			}
		}
	}
}

struct WsaBuf(WSABUF);

unsafe impl Send for WsaBuf {}
unsafe impl Sync for WsaBuf {}

struct SharedVariables {
	accept_unsafe:	unsafe extern "system" fn(usize, usize, *mut std::os::raw::c_void, u32, u32, u32, *mut u32, *mut WSAOVERLAPPED),
	iocp:			IOCompletionPort,
	server:			SOCKET,
	sites:			std::collections::HashMap<String, WsaBuf>,
	index:			WsaBuf,
}

impl SharedVariables {
	pub fn new(server: SOCKET, sites: std::collections::HashMap<String, WsaBuf>, index: WsaBuf) -> Self {
		const GUID: winapi::shared::guiddef::GUID = winapi::shared::guiddef::GUID {
			Data1:	0xb5367df1,
			Data2:	0xcbac,
			Data3:	0x11cf,
			Data4:	[0x95, 0xca, 0x00, 0x80, 0x5f, 0x48, 0xa1, 0x92]
		};
		let mut bytes = 0;
		let mut accept_unsafe = unsafe { std::mem::uninitialized() };
		unsafe { 
			WSAIoctl(server as _, SIO_GET_EXTENSION_FUNCTION_POINTER,
				std::mem::transmute(&GUID), std::mem::size_of::<winapi::shared::guiddef::GUID>() as _,
				&mut accept_unsafe as *mut _ as _, std::mem::size_of::<&()>() as _, &mut bytes, 0 as _, None)
		};

		Self { accept_unsafe, server, sites, index, iocp: IOCompletionPort::new(server) }
	}

	pub fn accept(&self, mut accept_data: Box<AcceptData>, listener: SOCKET) {
		*accept_data = Default::default();
		let mut size = 0;
		unsafe { (self.accept_unsafe)(listener, accept_data.sock, accept_data.data.as_mut_ptr() as _, 0, 32, 32, &mut size, Box::into_raw(accept_data) as _) };
	}

	fn worker(&self, len: u32, sock: SOCKET, overlapped: std::ptr::NonNull<WSAOVERLAPPED>) {
		if sock == self.server {
			let accept_data = unsafe { Box::from_raw(overlapped.as_ptr() as *mut AcceptData) };
			self.iocp.associate(accept_data.sock);
			OverlappedEx(Default::default()).recv(accept_data.sock);
			self.accept(accept_data, sock);
		} else {
			let mut overlapped_ex = OverlappedEx(unsafe { Box::from_raw(overlapped.as_ptr() as *mut OverlappedExInner) });
			match std::mem::replace(&mut overlapped_ex.0.operation, Default::default()) {
				Operation::RecvSize(_) if len == 0 => unsafe {
					closesocket(sock as _);
				}
				Operation::RecvSize(size) => {
					if size == 0 {
						self.on_recv(sock, overlapped_ex, Vec::new().into_boxed_slice())
					} else {
						let mut packet = Vec::with_capacity(size);
						unsafe { packet.set_len(size) };
						let mut wsabuf = WSABUF { len: size as _, buf: packet.as_ptr() as _ };
						let mut flags = MSG_WAITALL as u32;
						*overlapped_ex.0 = OverlappedExInner::new(Operation::Recv(packet.into_boxed_slice()));
						unsafe { WSARecv(sock as _, &mut wsabuf, 1, 0 as _, &mut flags, Box::into_raw(overlapped_ex.0) as _, None) };
					}
				}
				Operation::Recv(packet) => self.on_recv(sock, overlapped_ex, packet),
				_ => unsafe {
					closesocket(sock as _);
				}
			}
		}
	}
}

const SCRIPT_SIZE: usize = 49;
const SCRIPT: StaticPacket<[u8; SCRIPT_SIZE]> = StaticPacket {
	size:	SCRIPT_SIZE,
	data:	*include_bytes!("notfound.txt")
};


impl SharedVariables {
	fn on_recv(&self, client: SOCKET, overlapped_ex: OverlappedEx, packet: Box<[u8]>) {
		let empty = WsaBuf(WSABUF { buf: SCRIPT.as_ref().as_ptr() as _, len: SCRIPT.as_ref().len() as _ });
		overlapped_ex.send_static(client,
			if packet.is_empty() {
				&self.index
			} else {
				self.sites.get(unsafe { std::str::from_utf8_unchecked(&packet) }).unwrap_or(&empty)
			}
		);
	}
}

fn main() -> std::io::Result<()> {
	let sysinfo = get_sys_info();
	println!("processor count: {:?}", sysinfo.dwNumberOfProcessors);

	let mut sites = std::collections::HashMap::new();
	let mut buffer = Vec::default();
	let mut index = b"\0\0\0\0Main{main app_ui{app_ui.set(VBox(Label(\"Index\")".to_vec();
	{
		let mut functions = Vec::new();
		let mut counter = 0;
		let function = *b" self,app_ui{app_ui.goto(\"127.0.0.1/";/*{
			let mut function = b" self,app_ui{app_ui.goto(\"".to_vec();
			function.reserve(80);
			unsafe {
				let len = function.len();
				gethostname(function.get_unchecked_mut(len) as *mut _ as _, 80);
				let new_len = winapi::um::winbase::lstrlenA(function.get_unchecked(len) as *const _ as _) as usize + len;
				function.set_len(new_len);
				function.push(b'/');
			}
			function.into_boxed_slice()
		};*/

		for entry in std::fs::read_dir(".")? {
			let entry = entry?;
			if entry.file_type()?.is_file() {
				if let Ok(name) = entry.file_name().into_string() {
					if name.ends_with(".txt") {
						if let Ok(mut file) = std::fs::File::open(&name) {
							let metadata = entry.metadata()?;
							let begin = buffer.len();
							buffer.reserve(std::mem::size_of::<u32>() + metadata.len() as usize);
							let len: [u8; std::mem::size_of::<u32>()] = unsafe { std::mem::transmute(metadata.len() as u32) };
							buffer.extend_from_slice(&len);
							let end = file.read_to_end(&mut buffer)? + std::mem::size_of::<u32>();
							println!("{}", name);

							index.extend_from_slice(b",Button(\"");
							index.extend_from_slice(name.as_bytes());
							index.extend_from_slice(b"\", Main.f");
							index.extend_from_slice(counter.to_string().as_bytes());
							index.push(b')');
							functions.push(b'f');
							functions.extend_from_slice(counter.to_string().as_bytes());
							functions.extend_from_slice(&function);
							functions.extend_from_slice(name.as_bytes());
							functions.extend_from_slice(b"\")}");

							sites.insert(name, WsaBuf(WSABUF { buf: begin as _, len: end as _ }));

							counter += 1;
						}
					}
				}
			}
		}

		index.extend_from_slice(b"))}");
		index.extend_from_slice(&functions);
		index.push(b'}');
	}


	unsafe {
		*std::mem::transmute::<_, &mut u32>(index.get_unchecked_mut(0)) = (index.len() - std::mem::size_of::<u32>()) as _;
	}

	let index = index.into_boxed_slice();
	println!("{}", unsafe { std::str::from_utf8_unchecked(index.get_unchecked(4..)) });

	let buffer = buffer.into_boxed_slice();

	for wsabuf in sites.values_mut() {
		wsabuf.0.buf = unsafe { buffer.as_ptr().offset(wsabuf.0.buf as _) } as _;
	}

	let server = TcpListener::bind((Ipv4Addr::from(0), 0x6666))?.into_raw_socket();

	let thread_vars = std::sync::Arc::new(SharedVariables::new(server as _, sites, WsaBuf(WSABUF { len: index.len() as _, buf: index.as_ptr() as _})));

	for _ in 0..sysinfo.dwNumberOfProcessors - 1 {
		let mut cloned = std::sync::Arc::clone(&thread_vars);
		std::thread::Builder::new().spawn(move || {
			unsafe {
				let mut wsadata = std::mem::uninitialized();
				WSAStartup(0x202, &mut wsadata);
			}
			
			while let Some((len, socket, overlapped)) = cloned.iocp.get_queued_completion_status() {
				cloned.worker(len, socket, overlapped);
			}

			unsafe { WSACleanup() };
		})?;
	}

	for _ in 0..5 {
		thread_vars.accept(Default::default(), server as _);
	}

	while let Some((len, socket, overlapped)) = thread_vars.iocp.get_queued_completion_status() {
		thread_vars.worker(len, socket, overlapped);
	}

	Ok(())
}