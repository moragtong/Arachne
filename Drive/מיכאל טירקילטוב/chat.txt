Main {
	name_callback self, app_ui {
		name = app_ui.get(1, 0).get_text();
		self.sock.send(name);
		self.name = name;
		app_ui.set(
			VBox(
				Label("Name: " + name),
				ListView("name", "message"),
				HBox(Edit(), Button("send", Main.send_callback))					
			)
		);
	}

	send_callback self, app_ui {
		text = app_ui.get(1, 0).get_text();
		if text != "" {
			app_ui.get(0).insert_item("michael", 0);
			app_ui.get(0).set_item(text, 0, 1);
			app_ui.get(1, 0).set_text(none);
			self.send(text);
		};
	}

	recv_callback self, app_ui, sock, data {
		text = data.split("\0");
		app_ui.get(0).insert_item(data, 0);
		app_ui.get(0).set_item(text, 0, 1);
	}

	main app_ui {
		app_ui.set(
			VBox(
				Label("Enter your name:"),
				HBox(Edit(), Button("OK", Main.name_callback))
			)
		);
		obj = std.object(Main);
		obj.sock = app_ui.connection(127, 0, 0, 1, recv_callback);
		obj
	}
}