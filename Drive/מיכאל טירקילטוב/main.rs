extern crate winapi;

use winapi::shared::ws2def::SOCKADDR;
use winapi::shared::ws2def::WSABUF;
use winapi::shared::ws2def::SIO_GET_EXTENSION_FUNCTION_POINTER;
use winapi::shared::ws2def::AF_INET;
use winapi::um::winsock2::*;

use std::{
	ops::Deref,
	ops::DerefMut,
	os::windows::io::IntoRawSocket,
	net::*
};

fn get_sys_info() -> winapi::um::sysinfoapi::SYSTEM_INFO {
	unsafe {
		let mut sysinfo = std::mem::uninitialized();
		winapi::um::sysinfoapi::GetSystemInfo(&mut sysinfo);
		sysinfo
	}
}

#[derive(Debug)]
struct ArcInner<T: ?Sized> {
    strong:	std::sync::atomic::AtomicUsize,
    weak:	std::sync::atomic::AtomicUsize,
    data:	T,
}

#[derive(Debug)]
struct OwnedArc<T: ?Sized> {
	ptr:	Box<ArcInner<T>>
}

impl<T: ?Sized> OwnedArc<T> {
	/*pub fn new(obj: T) -> Self
	where T: Sized {
		Self {
			ptr:	unsafe { std::mem::transmute(std::sync::Arc::new(obj)) }
		}
	}*/

	pub fn into_arc(this: Self) -> std::sync::Arc<T> {
		unsafe { std::sync::Arc::from_raw(&(*Box::into_raw(this.ptr)).data) }
	}
}

impl<T: ?Sized> Deref for OwnedArc<T> {
	type Target = T;
	fn deref(&self) -> &T {
		&self.ptr.as_ref().data
	}
}

impl<T: ?Sized> DerefMut for OwnedArc<T> {
	fn deref_mut(&mut self) -> &mut T {
		&mut self.ptr.as_mut().data
	}
}

#[derive(Debug, Clone)]
struct SharedPacket(std::sync::Arc<[u8]>);

/*impl SharedPacket {
	pub fn data(&self) -> &[u8] {
		unsafe { self.0.get_unchecked(std::mem::size_of::<usize>()..) }
	}
}*/

#[derive(Debug)]
struct UniquePacket(OwnedArc<[u8]>);

impl UniquePacket {
	pub fn new(size: usize) -> Self {
		let buf = Vec::<u8>::with_capacity(size + std::mem::size_of::<ArcInner<usize>>());
		let mut fakeptr: Box<ArcInner<(usize, [u8])>> = unsafe { std::mem::transmute((buf.as_ptr(), size + std::mem::size_of::<usize>())) };
		std::mem::forget(buf);
		fakeptr.strong = std::sync::atomic::AtomicUsize::new(1);
		fakeptr.weak = std::sync::atomic::AtomicUsize::new(1);
		fakeptr.data.0 = size;
		let ptr = unsafe { std::mem::transmute(fakeptr) };
		UniquePacket(OwnedArc { ptr })
	}

	pub fn data(&self) -> &[u8] {
		unsafe { self.0.get_unchecked(std::mem::size_of::<usize>()..) }
	}

	/*pub fn data_mut(&mut self) -> &mut [u8] {
		unsafe { self.0.get_unchecked_mut(std::mem::size_of::<usize>()..) }
	}*/

	pub fn into_shared(self) -> SharedPacket {
		SharedPacket(OwnedArc::into_arc(self.0))
	}
}

struct AcceptData {
	sock:	SOCKET,
	data:	[u8; std::mem::size_of::<SOCKADDR>() * 2 + 32],
}

impl AcceptData {
	fn new(sock: SOCKET) -> Self {
		Self { sock, ..unsafe { std::mem::uninitialized() } }
	}
}

enum Operation {
	None,
	Accept(AcceptData),
	RecvSize(usize),
	Recv(UniquePacket),
	//Send(UniquePacket),
	SendShared(SharedPacket),
}

impl Default for Operation {
	fn default() -> Self {
		Operation::None
	}
}

#[repr(C)]
struct OverlappedExInner {
	overlapped: WSAOVERLAPPED,
	operation:	Operation,
}

impl OverlappedExInner {
	pub fn new(operation: Operation) -> Self {
		Self {
			overlapped:	unsafe { std::mem::zeroed() },
			operation,
		}
	}
}

impl Default for OverlappedExInner {
	fn default() -> Self {
		Self::new(Default::default())
	}
}

#[derive(Default)]
struct OverlappedEx(Box<OverlappedExInner>);

impl OverlappedEx {
	pub fn send_shared(&self, sock: SOCKET, packet: SharedPacket) {
		let mut wsabuf = WSABUF { len: packet.0.len() as _, buf: packet.0.as_ptr() as _ };
		unsafe {
			WSASend(sock as _, &mut wsabuf, 1, 0 as _, 0,
				Box::into_raw(Box::new(OverlappedExInner::new(Operation::SendShared(packet.clone())))) as _, None);
		}
	}

	/*pub fn send(mut self, sock: SOCKET, packet: UniquePacket) {
		let mut wsabuf = WSABUF { len: packet.0.len() as _, buf: packet.0.as_ptr() as _ };
		*self.0 = OverlappedExInner::new(Operation::Send(packet));
		unsafe { WSASend(sock, &mut wsabuf, 1, 0 as _, 0, Box::into_raw(self.0) as _, None) };
	}*/

	pub fn recv(mut self, sock: SOCKET) {
		*self.0 = OverlappedExInner::new(Operation::RecvSize(0));
		let mut wsabuf = {
			if let &mut Operation::RecvSize(ref mut size) = &mut self.0.operation {
				WSABUF { len: std::mem::size_of::<usize>() as _, buf: size as *mut _ as _ }
			} else {
				return
			}
		};

		let mut flags = MSG_WAITALL as u32;
		unsafe { WSARecv(sock as _, &mut wsabuf, 1, 0 as _, &mut flags, Box::into_raw(self.0) as _, None) };
	}
}

#[derive(Clone, Copy)]
struct IOCompletionPort(usize);

unsafe impl Send for IOCompletionPort {}

impl Default for IOCompletionPort {
	fn default() -> Self {
		IOCompletionPort(unsafe { winapi::um::ioapiset::CreateIoCompletionPort(-1 as _, 0 as _, 0, 0) } as _)
	}
}

impl IOCompletionPort {
	pub fn new(sock: SOCKET) -> Self {
		IOCompletionPort(unsafe { winapi::um::ioapiset::CreateIoCompletionPort(sock as _, 0 as _, sock as _, 0) } as _)
	}

	pub fn associate(&self, sock: SOCKET) {
		unsafe { winapi::um::ioapiset::CreateIoCompletionPort(sock as _, self.0 as _, sock as _, 0) };
	}

	pub fn get_queued_completion_status(&self) -> Option<(u32, SOCKET, OverlappedEx)> {
		unsafe {
			let (mut len, mut sock, mut overlapped) = std::mem::zeroed();
			winapi::um::ioapiset::GetQueuedCompletionStatus(self.0 as _, &mut len, &mut sock, &mut overlapped, winapi::um::winbase::INFINITE);
			if overlapped.is_null() {
				None
			} else {
				Some((len, sock, OverlappedEx(Box::from_raw(overlapped as _))))
			}
		}
	}
}

struct SharedVariables {
	accept_unsafe:	unsafe extern "system" fn(usize, usize, *mut std::os::raw::c_void, u32, u32, u32, *mut u32, *mut WSAOVERLAPPED),
	sockets:		std::sync::RwLock<Vec<usize>>
}

impl SharedVariables {
	pub fn new(sock: SOCKET) -> Self {
		const GUID: winapi::shared::guiddef::GUID = winapi::shared::guiddef::GUID {
			Data1:	0xb5367df1,
			Data2:	0xcbac,
			Data3:	0x11cf,
			Data4:	[0x95, 0xca, 0x00, 0x80, 0x5f, 0x48, 0xa1, 0x92]
		};
		let mut bytes = 0;
		let mut accept_unsafe = unsafe { std::mem::uninitialized() };
		unsafe { 
			WSAIoctl(sock as _, SIO_GET_EXTENSION_FUNCTION_POINTER,
				std::mem::transmute(&GUID), std::mem::size_of::<winapi::shared::guiddef::GUID>() as _,
				&mut accept_unsafe as *mut _ as _, std::mem::size_of::<&()>() as _, &mut bytes, 0 as _, None)
		};

		Self { accept_unsafe, sockets: Default::default() }
	}

	pub fn accept(&self, mut overlapped_ex: OverlappedEx, listener: SOCKET) {
		let sock = unsafe { socket(AF_INET, SOCK_STREAM, 0) };
		*overlapped_ex.0 = OverlappedExInner::new(Operation::Accept(AcceptData::new(sock)));
		let ptr = if let Operation::Accept(accept_data) = &mut overlapped_ex.0.operation {
			accept_data.data.as_mut_ptr()
		} else {
			return
		};
		let mut size = 0;
		unsafe { (self.accept_unsafe)(listener, sock, ptr as _, 0, 32, 32, &mut size, Box::into_raw(overlapped_ex.0) as _) };
	}

	fn worker(&self, len: u32, sock: SOCKET, mut overlapped_ex: OverlappedEx) {
		match std::mem::replace(&mut overlapped_ex.0.operation, Default::default()) {
			Operation::Accept(accept_data) => {
				{
					let mut sockets = self.sockets.write().unwrap();
					sockets.push(accept_data.sock);
				}
				self.accept(overlapped_ex, sock);
			}
			Operation::RecvSize(_) if len == 0 => self.on_close(sock),
			Operation::RecvSize(size) => {
				let packet = UniquePacket::new(size);
				let mut wsabuf = WSABUF { len: packet.data().len() as _, buf: packet.data().as_ptr() as _ };
				let mut flags = MSG_WAITALL as u32;
				*overlapped_ex.0 = OverlappedExInner::new(Operation::Recv(packet));
				unsafe { WSARecv(sock as _, &mut wsabuf, 1, 0 as _, &mut flags, Box::into_raw(overlapped_ex.0) as _, None) };
			}
			Operation::Recv(packet) => self.on_recv(sock, overlapped_ex, packet),
			_ => {}
		}
	}
}

impl SharedVariables {
	fn on_recv(&self, sock: SOCKET, overlapped_ex: OverlappedEx, packet: UniquePacket) {
		let packet = packet.into_shared();
		for target in self.sockets.read().unwrap().iter() {
			if *target != sock {
				OverlappedEx::default().send_shared(sock, packet.clone())
			}
		}
		overlapped_ex.recv(sock)
	}

	fn on_close(&self, client: SOCKET) {
		unsafe { closesocket(client as _) };
		let mut sockets = self.sockets.write().unwrap();
		let pos = sockets.iter().position(|&socket| socket == client as _).unwrap();
		sockets.remove(pos);
	}
}

fn main() -> std::io::Result<()> {
	let server = TcpListener::bind((Ipv4Addr::from(0), 0x7777))?.into_raw_socket();
	let iocp = IOCompletionPort::new(server as _);

	iocp.associate(server as _);

	let sysinfo = get_sys_info();
	println!("processor count: {:?}", sysinfo.dwNumberOfProcessors);

	let thread_vars = std::sync::Arc::new(SharedVariables::new(server as _));

	for _ in 0..sysinfo.dwNumberOfProcessors - 1 {
		let mut cloned = std::sync::Arc::clone(&thread_vars);
		std::thread::Builder::new().spawn(move || {
			unsafe {
				let mut wsadata = std::mem::uninitialized();
				WSAStartup(0x202, &mut wsadata);
			}
			
			while let Some((len, socket, overlapped)) = iocp.get_queued_completion_status() {
				cloned.worker(len, socket, overlapped);
			}
		})?;
	}

	for _ in 0..5 {
		thread_vars.accept(OverlappedEx(Default::default()), server as _);
	}

	while let Some((len, socket, overlapped)) = iocp.get_queued_completion_status() {
		thread_vars.worker(len, socket, overlapped);
	}

	Ok(())
}